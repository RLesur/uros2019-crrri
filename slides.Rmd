---
title: | 
  Headless Chr`r fontawesome::fa(name = "chrome", height= "32pt", fill = "#83929b")`me Automation with `r fontawesome::fa(name = "r-project", height= "48pt", fill = "#83929b")`
footer: | 
  crrri package --- Headless `r fontawesome::fa(name = "chrome", height= "12pt", fill = "#699dd3")` Automation with `r fontawesome::fa(name = "r-project", height= "12pt", fill = "steelblue")`
subtitle: "the crrri Package"
author: |
    Romain Lesur\
    [`r fontawesome::fa(name = "twitter-square", height= "16pt", fill = "#fff")`](https://twitter.com/RLesur) [`r fontawesome::fa(name = "github-square", height= "16pt", fill = "#fff")`](https://github.com/RLesur) [`r fontawesome::fa(name = "gitlab", height= "16pt", fill = "#fff")`](https://gitlab.com/RLesur) [`r fontawesome::fa(name = "linkedin", height= "16pt", fill = "#fff")`](https://www.linkedin.com/in/romain-lesur-381352/?locale=en_US)\
    [Deputy Head of the Statistical Service]{style="font-weight:lighter;font-size:0.7em"}
date: | 
    [![use of R in official statistics 2019 conference](https://raw.githubusercontent.com/uRosConf/unconfUROS2019/master/uRosConf2019.png){.shadowed width="80px"}](https://urosconf.org/)
couleur: "sable"
direction: false
lang: "fr"
output: 
  cgmj::diapo_pagedown:
    self_contained: false
    css: "https://raw.githubusercontent.com/marvelapp/devices.css/master/assets/devices.min.css"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{css}
.shadowed {
  filter: drop-shadow(-3px 5px 4px rgba(0,0,0,0.6));
}
.extrainfocontent {
  display: flex;
}
.extrainfocontent img, .extrainfocontent svg {
  vertical-align: middle;
}
.extrainfocontent svg {
  margin: 5px 0 10px 0;
}
h1 svg {
  vertical-align: middle;
}
.reprisetitre svg {
  vertical-align: top;
}
.level2 .figure {
  margin-left: 84mm;
}
.figure {
  display: flex;
  align-items: flex-end;
}
.figure img {
  box-shadow: 0 4px 10px rgb(0, 0, 0, 0.6), inset 0 0 3px rgb(0, 0, 0, 0.6);
}
.level2 p.caption {
  margin: 0 0 0 2mm;
  font-size:8pt;
  line-height: 1.2em;
  text-align: left;
}
li svg {
  vertical-align: sub;
}
```

## Web browser

### A web browser is like a shadow puppet theater

![
Suyash Dwivedi\
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)\
[via Wikimedia Commons
](https://commons.wikimedia.org/wiki/File:Tholpava_koothu_shadow_puppet_Ramayana_show_(1).jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Tholpava_koothu_shadow_puppet_Ramayana_show_%281%29.jpg/512px-Tholpava_koothu_shadow_puppet_Ramayana_show_%281%29.jpg){width="600"}


## Behind the scenes

### The puppet masters

![
Mr.Niwat Tantayanusorn, Ph.D.\
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)\
[via Wikimedia Commons
](https://commons.wikimedia.org/wiki/File:Ratchaburi_Shadow_Puppet_Museum_(Wat_Khanon)_3.jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Ratchaburi_Shadow_Puppet_Museum_%28Wat_Khanon%29_3.jpg/512px-Ratchaburi_Shadow_Puppet_Museum_%28Wat_Khanon%29_3.jpg){width="600"}

## What is a headless browser?

- Turn off the light: no visual interface
- Be the stage director... in the dark! ![Monkey Covering Eyes](https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/google/146/see-no-evil-monkey_1f648.png){width="32px" style="vertical-align:bottom;"}

![
Kent Wang from London, United Kingdom\
[CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0)\
[via Wikimedia Commons
](https://commons.wikimedia.org/wiki/File:A_collection_of_Nang_shadow_puppets_of_Thailand.jpg)
](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/A_collection_of_Nang_shadow_puppets_of_Thailand.jpg/512px-A_collection_of_Nang_shadow_puppets_of_Thailand.jpg){width="450"}

## Some use cases

- Responsible web scraping\
  (with JavaScript generated content)
- Webpages screenshots
- PDF generation
- Testing websites (or Shiny apps)

## Related packages

### Headless browser is an old topic

- {RSelenium} client for Selenium WebDriver, requires a Selenium server (Java).
- {webshot}, {webdriver} relies on the abandoned PhantomJS library.
- {hrbrmstr/htmlunit} uses the `HtmlUnit` Java library.
- {hrbrmstr/splashr} uses the `Splash` python library.
- {hrbrmstr/decapitated} uses headless Chrome command-line instructions or the Node.js gepetto module (built-on top of the puppeteer Node.js module)

## Headless Chr`r fontawesome::fa(name = "chrome", height= "20pt", fill = "#83929b")`me

### Since Chrome 59

- Basic tasks can be executed using command-line instructions
- Offers the possibility to have the full control of Chrome\
  using Node.js modules `puppeteer`, `chrome-remote-interface`...

### The crrri package<br/>developed with<br/>Christophe Dervieux<br/>WIP<br/><br/>[github.com/RLesur/crrri](https://github.com/RLesur/crrri)

- Have the full control of `r fontawesome::fa(name = "chrome", height= "16pt", fill = "#699dd3")` from `r fontawesome::fa(name = "r-project", height= "16pt", fill = "steelblue")` **without Java, Node or any server**
- Low-level API inspired by the [chrome-remote-interface](https://github.com/cyrus-and/chrome-remote-interface) JS module<br/>**give access to 500+ functions to control Chrome**
- Dedicated to advanced uses / R packages developers
- Compatible with Opera, EdgeHtml and Safari

## Technical explanations

Headless Chrome can be controlled using the \
**Chrome DevTools Protocol (CDP)**

### Steps to interact with headless Chrome
  
1. Launch Chrome in headless mode 
2. Connect R to Chrome through websockets
3. Build an asynchronous function that
    - sends CDP commands to Chrome
    - listen to CDP events from Chrome
4. Execute this async flow with R

The goal of {crrri} is to **ease these steps**.

## Chrome DevTools Protocol

### Program actions<br/>usually done with<br/>Chrome DevTools

![](https://gitlab.com/RLesur/uros2019-crrri/uploads/8b359fef67e545455f3b45ba0eac46c5/devtools.png){width="700"}

## Playing with headless Chr`r fontawesome::fa(name = "chrome", height= "20pt", fill = "#83929b")`me in RStudio 1.2

### `client` is a connection object<br/><br/><br/><br/>Inspect headless Chrome in RStudio

```r
remotes::install_github("rlesur/crrri")
library(crrri)
chrome <- Chrome$new() # launch headless Chrome
client <- chrome$connect(callback = ~.x$inspect())
```
![](https://gitlab.com/RLesur/uros2019-crrri/uploads/a094f9580a173ff687d3b43e9f515cd9/crrri1.png){width="600"}

## Chrome DevTools Protocol commands: an example

### A domain is a set of commands and events listeners

```r
Page <- client$Page # extract a domain
Page$navigate(url = "https://urosconf.org")
#> <Promise [pending]>
```

![](https://gitlab.com/RLesur/uros2019-crrri/uploads/b3fada55bf8929c95ade39d1d8d8ac48/crrri2.png){width="650"}

## An API similar to JavaScript

### All the functions are asynchronous

```r
Page$navigate(url = "https://urosconf.org")
#> <Promise [pending]>

```

An object of class Promise from the {promises} package.

### Chain with the appropriate pipe!{style="margin-top:2cm;"}

```r
Page$navigate(url = "https://urosconf.org") %...>%
  print()
#> $frameId
#> [1] "D1660E2ECC76A8356F78820F410BAA8C"

#> $loaderId
#> [1] "18180FE5BE9D9A60CC37F01610227729"
```

## Chaining commands and events listeners

### To receive events from Chrome most of domains need to be enabled 

```r
# ask Chrome to send Page domain events
Page$enable() %...>% { 
  # send the 'Page.navigate' command
  Page$navigate(url = "https://urosconf.org")
} %...>% {
  cat('Navigation starts in frame', .$frameId, '\n')
  # wait the event 'Page.frameStoppedLoading'
  # fires for the main frame 
  Page$frameStoppedLoading(frameId = .$frameId)
} %...>% {
  cat('Main frame loaded.\n')
}
```

**Chrome DevTools Protocol documentation**

[chromedevtools.github.io/devtools-protocol](https://chromedevtools.github.io/devtools-protocol/)

## Building higher level functions

### Write an asynchronous remote flow

```r
print_pdf <- function(client) {
  Page <- client$Page
  
  Page$enable() %...>% { 
    Page$navigate(url = "https://r-project.org/") 
    Page$loadEventFired() # await the load event
  } %...>% {
    Page$printToPDF() 
  } %...>% # await PDF reception
    write_base64("r_project.pdf") 
}
```
 
**Modify this script depending on the page content** (JS libraries...)

### Perform this flow synchronously in R

```r
perform_with_chrome(print_pdf)
```

## Headless Chrome features

### Frequent updates

- More than 40 domains, 400+ commands, 100+ events 
- Each release of Chrome brings new features.\
No need to update {crrri}: commands are fetched from Chrome.

### Features{.compact}

```{css}
.compact>ol>li, .compact>ul>li {
  margin-bottom: unset !important;
}
```

- DOM/CSS manipulation, extraction
- JavaScript Runtime (more than V8)
- Inspect/intercept network traffic
- Emulate devices
- Set JS bindings between Chrome and R
- PDF generation
- Screenshots
- Screencast...

## Example: emulate a device

### Screenshot your website with different devices<br/>

```r
iPhone8 <- function(client) {
  Emulation <- client$Emulation
  Page <- client$Page
  Emulation$setDeviceMetricsOverride(
    width = 375, height = 667, 
    mobile = TRUE, deviceScaleFactor = 2
  ) %...>% {
    Page$enable()  
  } %...>% {
    Page$navigate("https://rlesur.github.io/crrri")  
  } %...>% {
    Page$loadEventFired()
  } %>% 
    wait(3) %...>% {
    Page$captureScreenshot()  
  } %...>% 
    write_base64("iphone8.png")
}

perform_with_chrome(iPhone8)
```

<div style="transform: scale(0.43);transform-origin: 60px 130px;position:absolute;top:0;left:0;">
<div class="marvel-device iphone8 black">
<div class="top-bar"></div>
<div class="sleep"></div>
<div class="volume"></div>
<div class="camera"></div>
<div class="sensor"></div>
<div class="speaker"></div>
<div class="screen"><img src="https://gitlab.com/RLesur/uros2019-crrri/uploads/c191eeb28b01f1e25b746caf321e5b4f/iphone8.png"/ style="height: 667px"></div>
<div class="home"></div>
<div class="bottom-bar"></div>
</div>
</div>


## Example: screencast

### Navigate in RStudio 1.2 and record screencast<br/><br/>[see on Youtube](https://youtu.be/VFAhPAdzTFY)

![](https://gitlab.com/RLesur/uros2019-crrri/uploads/fa1ca69cfe9059a44d26aacefef6e981/screencast.png){width=750}

## Example: web scraping

```r
dump_DOM <- function(client) {
  Page <- client$Page
  Runtime <- client$Runtime
  Page$enable() %...>% {
    Page$navigate(url = 'https://github.com')
  } %...>% {
    Page$loadEventFired()
  } %>% 
    wait(3) %...>% {
    Runtime$evaluate(
      expression = 'document.documentElement.outerHTML'
    )
  } %...>% {
    writeLines(.$result$value, "gh.html")
  }
}

perform_with_chrome(dump_DOM)
```

## Conclusion

**{crrri} package**

### Pros {.compact}

- only one dependency: Chrome\
Java, Node or a server are not required
- easy to use with Travis or Docker
- integrates well with RStudio 1.2
- just update Chrome to get the latest features
- flexible: define your own flow
- compatible with Shiny (because of the {promises} package)<br/>orchestrate headless Chrome in your Shiny app!

### Cons {.compact}

- low-level interface: Chrome DevTools Protocol is highly technical
- mostly dedicated to R developers/hackers

## Credits

### Thanks to

- Miles McBain for [chradle](https://github.com/MilesMcBain/chradle)
- Bob Rudis for [decapitated](https://gitlab.com/hrbrmstr/decapitated)
- Andrea Cardaci for [chrome-remote-interface](https://github.com/cyrus-and/chrome-remote-interface)
- Marvelapp for [devices.css](http://marvelapp.github.io/devices.css/)

**Questions?**

_This deck was made with [pagedown](https://github.com/rstudio/pagedown) and is licensed under [![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png){style="border-width:0;vertical-align:text-top;"}](http://creativecommons.org/licenses/by-sa/4.0/)_
